# Bura menu

Console interface for SLURM workload manager made for supercomputer "Bura" that is located at the Univesitry of Rijeka.
For more information about Bura click this [link](https://wbc-rti.info/mobile/object_view/16059).

## User interface

![Main menu](https://gitlab.com/ppavacic/res/-/raw/main/Bura/main-menu-bura.png)

Allows:
 - setting SLURM settings, node, partition of supercomputer to run task/s
 - getting general system/SLURM run information
 - setting up pyenv Python versions
 - saving settings in a configuration file which can be loaded later
 - running applications using SLURM workload manager
